/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * @author Aly Cardinal
 * @link https://vk.com/id650525154 
 * @community https://vk.com/a.home846
 * 
*/

#include "main.h"
#include "multiplayer.h"

void multiplayer::Init_13()
{
    InitHooks_13();
    InitMemoryCopies_13();
}

void multiplayer::InitHooks_13()
{
    InitHooks_Weapons();
}

void multiplayer::InitMemoryCopies_13()
{

}