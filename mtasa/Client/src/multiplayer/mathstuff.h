#ifndef MATHSTUFF_H
#define MATHSTUFF_H

// Clamps a value between two other values ( min < a < max )
template <class T>
T Clamp(const T& min, const T& a, const T& max)
{
    return a < min ? min : a > max ? max : a;
}

// Checks whether a value is between two other values ( min <= a <= max )
template <class T>
bool Between(const T& min, const T& a, const T& max)
{
    return a >= min && a <= max;
}

// Lerps between two values depending on the weight
template <class T>
T Lerp(const T& from, float fAlpha, const T& to)
{
    return (T)((to - from) * fAlpha + from);
}

// Find the relative position of Pos between From and To
inline const float Unlerp(const double dFrom, const double dPos, const double dTo)
{
    // Avoid dividing by 0 (results in INF values)
    if (dFrom == dTo)
        return 1.0f;

    return static_cast<float>((dPos - dFrom) / (dTo - dFrom));
}

// Unlerp avoiding extrapolation
inline const float UnlerpClamped(const double dFrom, const double dPos, const double dTo) { return Clamp(0.0f, Unlerp(dFrom, dPos, dTo), 1.0f); }

#endif // MATHSTUFF_H