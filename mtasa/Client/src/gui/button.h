#ifndef BUTTON_H
#define BUTTON_H

#include "window.h"

class button
{
private:
    float g_fPosX;
    float g_fPosY;
    float g_fSizeX;
    float g_fSizeY;
    ImColor g_bgColor, g_textColor;
    bool g_bShow;    
    uint32_t g_ID;
    char g_text[0xFF];
    uint8_t g_iButtonTouchEx;
public:
    button(uint32_t, const char* text, float, float, float, float, ImColor, ImColor, bool);
    ~button();
    
    bool checkIn(float x, float y);
    void draw(window*);

    void pushTouch(float x, float y);
    void popTouch(float x, float y);
    void moveTouch(float x, float y);

    bool isHovered();
};

#endif // BUTTON_H