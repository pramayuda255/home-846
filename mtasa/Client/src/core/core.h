#ifndef CORE_H
#define CORE_H

#include "../game/game.h"
#include "modmanager.h"

class core
{
public:
    // Mod manager
    static modmanager* m_pModManager;

    // Module interfaces.
    static game* m_pGame;
public:
    static void initialize();
    static void OnCrashAverted(uint uiId);
    static void OnEnterCrashZone(uint uiId);
    static bool GetDebugIdEnabled(uint uiDebugId);
    static void LogEvent(uint uiDebugId, const char* szType, const char* szContext, const char* szBody, uint uiAddReportLogId);
    static game* getGame();
    static modmanager* getModManager();
};

#endif // CORE_H