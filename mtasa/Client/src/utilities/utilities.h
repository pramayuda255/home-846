#ifndef UTILITIES_H
#define UTILITIES_H

// types
typedef unsigned long DWORD;
typedef unsigned char BYTE;
typedef unsigned long dword;
typedef unsigned char byte;

class utilities
{
private:
     static char* g_szStorage;
public:
     static uint32_t getTickCount();
     static uintptr_t getLibraryHandle(const char*);
     static void setClientStorage(char*);
     static char* getClientStorage();
     static uintptr_t loadTextureFromTxd(const char* database, const char *texture);
     static uintptr_t loadTextureFromDB(const char* dbname, const char *texture);
     static uintptr_t findMethod(const char *libname, const char *name);
     static unsigned elfhash(const char *_name);
     static uint32_t gnuhash(const char *_name);
     static void cpToUtf8(char *out, const char *in, unsigned int len = 0);
};

#endif // UTILITIES_H
