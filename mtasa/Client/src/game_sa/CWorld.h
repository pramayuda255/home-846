#pragma once

#include "../main.h"

class CWorld
{
private:
    /* data */
public:
    static void Add(uintptr_t entity);
    static void Remove(uintptr_t entity);
};
